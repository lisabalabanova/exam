#include <iostream>
#include <cstdlib> 
#include <iostream>

#include <openssl/conf.h> // �������, ��������� � ��������� ��������� OpenSSL
#include <openssl/conf.h>
#include <openssl/evp.h> // ���� ���������������� ������� https://wiki.openssl.org/index.php/EVP
#include <openssl/err.h> // ���� ���������� ������ OpenSSL � �� �����������
#include <openssl/aes.h>
#include <fstream>

#pragma comment (lib, "ws2_32.LIB")
#pragma comment (lib, "gdi32.LIB")
#pragma comment (lib, "advapi32.LIB")
#pragma comment (lib, "crypt32")
#pragma comment (lib, "user32")
#pragma comment (lib, "wldap32")
//
//extern "C" _declspec(dllimport) int RSA_public_encrypt(int flen, const unsigned char *from, const unsigned char *to, RSA *rsa, int padding);

// ���������� OpenSSL (openssl.org) ������������ ������ ����������� (��. �������� ���. �� �����������)

using namespace std;


// ������� ��� ������ ����� 
void bruteforce() {
	EVP_CIPHER_CTX *ctx; // structure
	ctx = EVP_CIPHER_CTX_new(); // �������� ��������� � ����������� ������
	int len = 256;
	unsigned char *iv = (unsigned char *)"0123456789012345"; // ���������������� ������, �����������
	unsigned char *pass_to_find = (unsigned char *)"{\r\n"; // ��� ������ ���������� ���� (�� �������)

	// ���������� ��� ��������� ����� (000..1000 - 00...9999)
	for (int i = 1000; i <= 9999; i++) {
		char key[256];
		unsigned char decryptedtext[256]; // ���� ����� ���������� �������������� ���������
		sprintf_s(key, 33, "%032d", i);
		unsigned char *key2 = (unsigned char *)key; // ��������������� ��� (key ����� ��� char * ��� ������� sprintf_s, key2 ����� ����� ��� unsigned char * ��� ������� EVP_DecryptInit_ex
		EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, (unsigned char *)key2, iv);// ������������� ������� AES, ������ � ��������


		std::fstream deshef_input;
		deshef_input.open("2_encrypted.txt", std::fstream::in | std::fstream::binary); // ��������� ���� 2_encrypted.txt
		char str2[256]; // ���� ����� ��������� ��� ����
		deshef_input.read(str2, 256); // �������� ������ 256 ��� �� ����� 2_encrypted.txt
		EVP_DecryptUpdate(ctx,
			decryptedtext,
			&len,
			(unsigned char *)str2,
			deshef_input.gcount());  // ���������

		deshef_input.close(); // ��������� ���� 2_encrypted.txt

		// �������� �������������� ������
		// ���� ������ ��������������� ����� ����� "{\r\n" ������ ���� ������
		if ((decryptedtext[0] == pass_to_find[0]) && (decryptedtext[1] == pass_to_find[1]) && (decryptedtext[2] == pass_to_find[2])) {
			std::cout << "���� ������:\n";
			// ������� ����
			for (int t = 0; t < 33; t++) {
				std::cout << key[t];
			}
			cout << endl;
		}
	};
}

// ������� ��� ������������� ����� 
void decrypte_file() {
	EVP_CIPHER_CTX *ctx; // structure
	ctx = EVP_CIPHER_CTX_new(); // �������� ��������� � ����������� ������
	int len = 33;
	unsigned char *iv = (unsigned char *)"0123456789012345"; // ���������������� ������, �����������
	unsigned char *key = (unsigned char *)"00000000000000000000000000009530"; // ����, ������� �� �����
	unsigned char decryptedtext[256]; // �������������� ���������
	EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, (unsigned char *)key, iv);// ������������� ������� AES, ������ � ��������


	std::fstream deshef_input;
	deshef_input.open("2_encrypted.txt", std::fstream::in | std::fstream::binary); // ��������� ���� 2_encrypted.txt
	std::fstream deshef_output;
	deshef_output.open("output.txt", std::fstream::out | std::fstream::trunc | std::fstream::binary); // ��������� ���� output.txt
	char str2[256]; // ���� ����� ��������� ��� ����
	deshef_input.read(str2, 256); // �������� ������ 256 ��� �� ����� 2_encrypted.txt
	while (deshef_input.gcount() > 0) { // ���� ����� ���� ��� �� ������� ������ ��� 0 �� ����� �������������� ������
		EVP_DecryptUpdate(ctx,
			decryptedtext,
			&len,
			(unsigned char *)str2,
			deshef_input.gcount());  // ���������

		deshef_output.write((char *)decryptedtext, len); // ���������� �������������� ����� � ���� output.txt
		deshef_input.read(str2, 256); // ��������� ���� 256 ��� ����� � ���� � ������ ����� (while)
	}

	EVP_DecryptFinal_ex(ctx, decryptedtext, &len);

	deshef_output.write((char*)decryptedtext, len); // ���������� �������������� ������� � ���� output.txt
	// ��������� �����
	deshef_output.close();
	deshef_input.close();

}

int main()
{
	setlocale(LC_ALL, "Rus");
	// �������� ������� bruteforce ��� ������ �����
	bruteforce();
	// �������� ������� decrypte_file ��� ��������� ��������������� �����
	decrypte_file();
	system("pause");
	return 0;
}


